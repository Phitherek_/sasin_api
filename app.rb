# frozen_string_literal: true

require 'sinatra/base'
require_relative 'lib/sasin_api'

module SasinApi
  class App < Sinatra::Application
    post '/convert' do
      service = SasinApi::Services::Performers::Convert.new
      service.call(request)
    end

    run! if app_file == $PROGRAM_NAME
  end
end
