# frozen_string_literal: true

require 'sasin'
require 'oj'

module SasinApi
  module Services
    module Performers
      class Convert
        attr_reader :status, :body, :headers, :result

        def initialize
          @status = 200
          @headers = { 'Content-Type' => 'application/json' }
        end

        def call(request)
          parsed_body = Oj.load(request.body)
          if parsed_body['input'].nil? || parsed_body['input'] == ''
            @status = 422
            @body = { error: 'Parameter input can\'t be blank' }
          else
            @body = { result: Sasin::Converter.convert(parsed_body['input']) }
          end
          @body = @body.to_json
          @result = [@status, @headers, @body]
        end
      end
    end
  end
end
