# Sasin API

This is a simple Sinatra web app that implements the API for converting to an universal sasin unit.

## Setup

* `bundle install`
* `rackup`

## Contributing

* Fork the repository
* Add changes to a separate branch
* Run `rspec spec`
* Run `rubocop -A`
* Commit changes
* Open merge request

## License

The app is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Disclaimer

Yes, this app is kind of a joke. It refers to a Polish minister of assets Jacek Sasin that spent a 70 million of Polish zloty on an election that didn't finally happen.