# Sasin API documentation

This API does not need any kind of authentication.

## POST `/convert`

* Request type: `application/json`
* Response type: `application/json`
* Request:
```json
{
  "input": 1
}
```
The `input` parameter can be a number or a string.
* Response:
```json
{
  "result": 1.4285714286e-08
}
```
* Error response when input is missing or empty:
```json
{
  "error": "Parameter input can't be blank"
}
```