# frozen_string_literal: true

require 'spec_helper'
require 'oj'
require 'rack'
require_relative '../../../../../lib/sasin_api/services/performers/convert'

RSpec.describe SasinApi::Services::Performers::Convert do
  describe '#call' do
    it 'should properly convert to sasin' do
      expected_status = 200
      expected_headers = { 'Content-Type' => 'application/json' }
      expected_body = { result: 1.4285714286e-08 }.to_json
      service = described_class.new
      request = Rack::Request.new(Rack::MockRequest.env_for('http://localhost:9292/convert', {
                                                              'HTTP_CONTENT_TYPE' => 'application/json',
                                                              'HTTP_ACCEPT' => 'application/json',
                                                              input: { input: 1 }.to_json
                                                            }))
      expect(service.call(request)).to eq([expected_status, expected_headers, expected_body])
      expect(service.status).to eq(expected_status)
      expect(service.headers).to eq(expected_headers)
      expect(service.body).to eq(expected_body)
    end

    it 'should return proper error response' do
      expected_status = 422
      expected_headers = { 'Content-Type' => 'application/json' }
      expected_body = { error: 'Parameter input can\'t be blank' }.to_json
      service = described_class.new
      request = Rack::Request.new(Rack::MockRequest.env_for('http://localhost:9292/convert', {
                                                              'HTTP_CONTENT_TYPE' => 'application/json',
                                                              'HTTP_ACCEPT' => 'application/json',
                                                              input: { input: '' }.to_json
                                                            }))
      expect(service.call(request)).to eq([expected_status, expected_headers, expected_body])
      expect(service.status).to eq(expected_status)
      expect(service.headers).to eq(expected_headers)
      expect(service.body).to eq(expected_body)
    end
  end
end
